# Simple class to load and search translations in file

import json

class i18n:
    lang = None
    jTree = None
    def __init__(self, l='es'):
        self.lang = l
        f = open('lang/{lang}.json'.format(lang=l), 'r', encoding='utf-8')
        self.jTree = json.load(f)

    def translate(self, key):
        if self.jTree == None:
            return key
        else:
            splt = key.split('.')
            iRes = self.__searchTrad(self.jTree, splt)
            if not iRes == None:
                return iRes
            else:
                return key
    
    def __searchTrad(self, tree, segments):
        if len(segments) == 1: # final level
            k = segments.pop(0)
            res = None
            if k in tree:
                if not tree[k] == None and type(tree[k]) is str:
                    res = tree[k]
            else:
                return None
            return res
        else: # next level
            k = segments.pop(0)
            if k in tree:
                if tree[k] == None:
                    return None
                else:
                    return self.__searchTrad(tree[k], segments)
            else:
                return None
