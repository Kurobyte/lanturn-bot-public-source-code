cfg = {
    'cmd_prefix': '$',
    'bot_token': '', # Discord token id
    'bot_name': '', #Name of your discord bot
    'switch_ip': '192.168.1.000', # Switch IP
    'switch_port': 6000,
    'admin_ids': [], # List of IDs of your admins
    'bot_lang': 'en',
    'ign': '', # In game name of your bot
    #Set int 4 digit number to fix that digits ex: 1110 -> means 111 and  the last can be [0 to 9]
    #Set None to full random code
    'trade_code': None
}

raidCmd = {
    'CheckMySeed': 'seed',
    'CheckQueueSize': 'qsize',
    'checkMyPlace': 'myplace',
    'GetSeed': 'getseed',
    'GetFrameData': 'framedata',
    'CalcDate': 'calcdate'
}