from discord.ext import tasks, commands
import discord
from framecalc import *
from seedgen import *
from GetPokeInfo import *
from bot import *
from Person import *
from ArrayQueue import *
import time
from datetime import datetime, timedelta, date
from botConfig import raidCmd, cfg

# 300 with the current queue and the reporting system
# will make sure everyone has a place and can see when they will be served
# q = ArrayQueue(300)

# until possible merge and improvement, setting it to 20 as from the previous commits
q = ArrayQueue(20)

class RaidCommands(commands.Cog):
	def __init__(self, client):
		self.checkDataReady.start()
		self.userChannel = None
		self.user = None
		self.id = None
		self.person = None
		self.idInt = None

	#Clears instance variables
	def clearData(self):
		self.userChannel = None
		self.user = None
		self.id = None
		self.idInt = None
		self.person = None

	#Generates the appropriate string based on your star and square frames
	def generateFrameString(self, starFrame, squareFrame):
		global t
		starFrameMessage = ""
		if starFrame != -1:
			starFrameMessage = str(starFrame + 1)
		else:
			starFrameMessage = t('serving.shiny_away')

		squareFrameMessage = ""
		if squareFrame != -1:
			squareFrameMessage = str(squareFrame + 1)
		else:
			squareFrameMessage = t('serving.shiny_away')

		return starFrameMessage, squareFrameMessage

	#Reports how many people are in the queue
	@commands.command(name=raidCmd["CheckQueueSize"])
	async def checkQueueSize(self, ctx):
		await ctx.send(t('qsize.status').format(size=str(q.size() ) ) )

	#Reports where the sender is in the queue
	@commands.command(name=raidCmd["checkMyPlace"])
	async def checkMyPlace(self, ctx):
		global q, t
		id = ctx.message.author.id
		p = Person(id, ctx.message.channel, ctx.message.author)
		place = q.indexOf(p) + 1

		if place > 0:
			await ctx.send(t('myplace.position').format(id=str(id), place=place))
			# await p.send("```python\nEi! eres el " + place + " en la cola.\n```")
		else:
			await ctx.send(t('myplace.notinqueue'))

	@commands.command(name=raidCmd["CheckMySeed"])
	async def checkMySeed(self, ctx):
		global q, t

		if q.availableSpace():
			print("Invoked by: " + str(ctx.message.author) + " in: " + str(ctx.message.guild))
			if ctx.message.guild != None:

				#Constructs person object for queue
				id = ctx.message.author.id
				p = Person(id, ctx.message.channel, ctx.message.author)

				#Checks if queue already contains assets from the constructed person object
				if not q.contains(p) and self.idInt != id:

					#Checks if anyone is currently being served
					if self.person == None:
						q.enqueue(p)
						await ctx.send(t('seed.started_alone').format(id=str(id)))

					#Checks if you are already being served
					elif self.person.getID() != id:
						q.enqueue(p)

						#for correct grammar
						prsn = ""
						pre = ""
						if q.size() == 1:
							prsn = t('seed.person')
						else:
							prsn = t('seed.people')

						await ctx.send(t('seed.started').format(id=str(id), size=str(q.size()), persons=prsn))
					elif self.person.getID() == id:
						await ctx.send(t('seed.nowserving'))
				else:
					await ctx.send(t('seed.inqueue'))
		else:
			await ctx.send(t('seed.fullqueue'))

	#Main loop that is sending and receiving data from the dudu client
	@tasks.loop(seconds=0.1)
	async def checkDataReady(self):
		global q

		#If there is no person being served and the queue is not empty, get the next person in the queue
		#and start the dudu client
		if self.person == None and not q.isEmpty():
			self.person = q.dequeue()
			print("Current person being served: " + str(self.person.getUser()))
			initializeDuduClient()

		#Checks if lanturn is now searching and if there is a person being served
		if checkSearchStatus() and self.person != None:

			#assigns assets based on the person being served
			self.userChannel = self.person.getUserChannel()
			self.user = self.person.getUser()
			self.id = self.person.getIDString()
			self.idInt = self.person.getID()
			
			#Gets link code from text file
			code = getCodeString()

			await self.userChannel.send(t('serving.code_send').format(id=self.id,ign=cfg['ign']))
			await self.user.send("```python\n{translate}```".format(translate=t('serving.private').format(code=code)))

		#Check if user has timed out and checks if a valid userChannel is present
		if checkTimeOut() and self.userChannel != None:
			await self.userChannel.send(t('serving.timeout').format(id=self.id, size=str(q.size())) )
			self.clearData()

		#Check if a valid user channel is present and if the dudu client is still running
		if self.userChannel != None and not checkDuduStatus():
			time.sleep(2.0)
			ec, pid, seed, ivs, iv = getPokeData()

			if seed != -1:
				calc = framecalc(seed)
				starFrame, squareFrame = calc.getShinyFrames()
				startDate = datetime((date.today()).year, 1, 1) # Get day 1 of the actual year to set a pivot

				stFrameDate = ''
				sqFrameDate = ''
				hasFecha = ''

				if starFrame != -1:
					stFrameDate = '({date})'.format(date=(startDate + timedelta(days=(starFrame + 1))).strftime("%d/%m/%Y"))
				
				if squareFrame != -1:
					sqFrameDate = '({date})'.format(date=(startDate + timedelta(days=(squareFrame + 1))).strftime("%d/%m/%Y"))

				if starFrame != -1 or squareFrame != -1:
					hasFecha = '\n' + t('serving.start_date').format(date=startDate.strftime("%d/%m/%Y"))

				starFrameMessage, squareFrameMessage = self.generateFrameString(starFrame, squareFrame)

				await self.userChannel.send(str(
					self.id + "```python \n{ec}: " + str(hex(ec)) +
					"\n{pid} " + str(hex(pid)) +
					"\n{seed} " + seed +
					"\n{l_ivs} " + str(ivs) +  
					"\n{ivs} " + str(iv[0]) + "/" + str(iv[1]) + "/" + str(iv[2]) + "/" + str(iv[3]) + "/" + str(iv[4]) + "/" + str(iv[5]) + 
					"\n{star} " + starFrameMessage + "  " + stFrameDate +
					"\n{square} " + squareFrameMessage + "  " + sqFrameDate + hasFecha + "```").format(
						ec=t('serving.ec'),
						pid=t('serving.pid'),
						seed=t('serving.seed'),
						l_ivs=t('serving.ivs_list'),
						ivs=t('serving.ivs'),
						star=t('serving.star'),
						square=t('serving.square')
						)
					)

				#outputs how many people remain in line
				time.sleep(1.0)
				await self.userChannel.send(t('serving.resting').format( size=str(q.size()) ))
				self.clearData()
			else:
				await self.userChannel.send(t('serving.invalid').format(id=self.id, size=str(q.size())) )
				self.clearData()

			
		#await ctx.send("Invoked")

	@commands.command(name=raidCmd['CalcDate'])
	async def calcDate(self, ctx, arg1=None, arg2=None):
		global t
		try:
			# print(arg1, arg2)
			pivot = None
			days = None
			if arg1 == None:
				raise Exception()
			else:
				days = int(arg1)

			if not arg2 == None:
				pDate = [ int(dt) for dt in arg2.split("/") ]
				pivot = datetime(pDate[2], pDate[1], pDate[0])

			else:
				pivot = datetime((date.today()).year, 1, 1)
			
			await ctx.send(t('calc_date.result').format(date=(pivot + timedelta(days=days)).strftime("%d/%m/%Y"), pivot=pivot.strftime("%d/%m/%Y")))

		except:
			await ctx.send(t('calc_date.error').format(pre=cfg['cmd_prefix'], cmd=raidCmd['CalcDate']))

	@commands.command(name=raidCmd['GetSeed'])
	async def obtainSeed(self, ctx, arg1=None, arg2=None, arg3=None):
		try:
			#Convert user strings to a usable format (int)
			ec = int(arg1, 16)
			pid = int(arg2, 16)
			ivs = [ int(iv) for iv in arg3.split("/") ]

			#Generate seed from user input
			gen = seedgen()
			seed, ivs = gen.search(ec, pid, ivs)

			#Calculate star and square shiny frames based on seed
			calc = framecalc(seed)
			starFrame, squareFrame = calc.getShinyFrames()

			#Format message based on result and output
			starFrameMessage, squareFrameMessage = self.generateFrameString(starFrame, squareFrame)

			await ctx.send("```python\nRaid seed: " + str(seed) + "\nAmount of IVs: " + str(ivs) + "\nStar Shiny at Frame: " + starFrameMessage + "\nSquare Shiny at Frame: " + squareFrameMessage + "```")
		except:
			await ctx.send("Please format your input as: ```$GetSeed [Encryption Constant] [PID] [IVs as HP/Atk/Def/SpA/SpD/Spe]```")

	@commands.command(name=raidCmd['GetFrameData'])
	async def obtainFrameData(self, ctx, arg1=None):
		try:
			#Convert user strings to a usable format
			seed = hex(int(arg1, 16))

			#Calculate star and square shiny frames based on seed
			calc = framecalc(seed)
			starFrame, squareFrame = calc.getShinyFrames()

			#Format message based on result and output
			starFrameMessage, squareFrameMessage = self.generateFrameString(starFrame, squareFrame)

			await ctx.send("```python\nFor Seed: " + str(seed) + "\nStar Shiny at Frame: " + starFrameMessage + "\nSquare Shiny at Frame: " + squareFrameMessage + "```")
		except:
			await ctx.send("```$GetFrameData [Input your Seed]```")
		

def setup(client):
	client.add_cog(RaidCommands(client))