import discord
from discord.ext import commands
from botConfig import cfg
from i18n import i18n
import asyncio
import sys

TOKEN = cfg['bot_token']
trad = i18n(cfg['bot_lang']) # Load the translation set in config
t = trad.translate # Export translate as a global t method

client = commands.Bot(command_prefix = cfg['cmd_prefix'])
client.remove_command('help')

extensions = ['RaidCommands']

@client.event
async def on_ready():
	print('Logged in as')
	print(client.user.name)
	print(client.user.id)
	print('------')

@client.event
async def on_message(message):
	if message.author == client.user:
		return
	channel = message.channel
	if message.guild is None:
		await message.channel.send(t('generic.hey'))
	else:
	    await client.process_commands(message)

#Command used for bot admin to turn their bot off
#Please put the admin's discord ID where indicated
@client.command()
async def logout(ctx):
	if ctx.message.author.id in cfg['admin_ids']:
		await ctx.send('```{text}```'.format(text=t('generic.turnoff')))
		await client.logout()
	else:
		await ctx.send( t('generic.turnoff_noadmin') )
		
#Sends greet command
@client.command()
async def greet(ctx):
	await ctx.send( t('generic.greet').format(botName=cfg['bot_name']) )

@client.command()
async def load(extension):
	try:
		client.load_extension(extension)
		print('Loaded {}'.format(extension))
	except Exception as error:
		print('{} cannot be loaded. [{}]'.format(extension, error))

@client.command()
async def unload(extension):
	try:
		client.unload_extension(extension)
		print('Unloaded {}'.format(extension))
	except Exception as error:
		print('{} cannot be unloaded. [{}]'.format(extension, error))

async def test():
	while True:
		print("Hello!")
		await asyncio.sleep(1)

if __name__ == '__main__':
	for extension in extensions:
		try:
			client.load_extension(extension)
		except Exception as error:
			print('{} cannot be loaded. [{}]'.format(extension, error))

	#client.loop.create_task(test())
	client.run(TOKEN)
